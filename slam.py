#!/usr/bin/env python
# -*- coding: utf-8 -*-

import base64
import threading
import math
import multiprocessing
import numpy as np
import random


class FeaturePointDepth(object):
    CAM_BF_PARAM = 53.0  # Calculated by calibrator.
    CAM_WIDTH_PAR_DEPTH = 0.99 / 1.33
    '''
        <-----0.99m----->
                ^
                1.33m
                v
                cam
    '''
    # TODO(takiyu): Calibration for ps3eye

    HUGE_GRAD = 1000000
    MAX_GRAD_ABS = 0.025

    def __init__(self):
        import cv2

        # Oriented-BRIEF
        self.orb = cv2.ORB()
        # Matcher
        self.bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)

    def detect(self, img1, img2, n_max=-1):
        def gradient(x1, y1, x2, y2):
            denom = x2 - x1
            if denom == 0:
                return self.HUGE_GRAD
            return (y2 - y1) / denom

        # Find the keypoints
        kp1, des1 = self.orb.detectAndCompute(img1, None)
        kp2, des2 = self.orb.detectAndCompute(img2, None)
        if des1 is None or des2 is None:
            return None

        # Matching
        matches = self.bf.match(des1, des2)

        # Sort them in the order of their distance.
        matches = sorted(matches, key=lambda x: x.distance)

        pt1s = list()
        pt2s = list()
        depths = list()
        # for each matching point
        count = 0
        for m in matches:
            x1, y1 = kp1[m.queryIdx].pt
            x2, y2 = kp2[m.trainIdx].pt
            # gradient check
            grad = gradient(x1 + img1.shape[1], y1, x2, y2)
            if grad < -self.MAX_GRAD_ABS or self.MAX_GRAD_ABS < grad:
                continue

            # points
            pt1s.append((x1, y1))
            pt2s.append((x2, y2))

            # depth
            dx = abs(x2 - x1)
            if dx == 0:
                depth = float('inf')
            else:
                depth = self.CAM_BF_PARAM / dx
            depths.append(depth)

            # counter check
            count += 1
            if count == n_max:
                break
        return pt1s, pt2s, depths

    def cvt_ground(self, image, pts, depths):
        ground_points = list()
        for pt, depth in zip(pts, depths):
            ground_x = self._calc_ground_x(image.shape[1], pt[0], depth)
            ground_points.append((ground_x, depth))
        return ground_points

    def _calc_ground_x(self, im_width, x_coord, depth):
        real_width = self.CAM_WIDTH_PAR_DEPTH * depth
        x_rate = x_coord / im_width  # x / width [0:1]
        x_rate -= 0.5  # [-0.5, -0.5]
        return real_width * x_rate


# Debugger
def draw_depth_points(img, points, depths):
    import cv2
    out = img.copy()

    def clamp(x, a, b):
        return max(min(x, b), a)

    for pt, dist in zip(points, depths):
        r = clamp(dist - 0.0, 0.0, 1.0) * 255
        g = clamp(dist - 1.0, 0.0, 1.0) * 255
        b = clamp(dist - 2.0, 0.0, 1.0) * 255
        color = (b, g, r)
        cv2.circle(out, (int(pt[0]), int(pt[1])), 2, color, -1)

    return out


def bound_angle(phi):
    ''' Bound angle [-pi,pi] '''
    if 0.0 <= phi:
        phi = math.fmod(phi, 2.0 * math.pi)
        if phi > math.pi:
            phi -= 2.0 * math.pi
    else:
        phi = math.fmod(phi, -2.0 * math.pi)
        if phi < -math.pi:
            phi += 2.0 * math.pi
    # assert -pi <= phi and phi <= pi
    return phi


def local_to_global(local_x, local_y, pose):
    distance = math.sqrt(local_x ** 2 + local_y ** 2)
    theta = math.atan2(local_x, local_y) + pose[2]
    global_x = distance * math.cos(theta) + pose[0]
    global_y = distance * math.sin(theta) + pose[1]
    return global_x, global_y


class GridMap(object):
    DELTA = 0.0001
    MAX_DEPTH_SQ = 8.0 ** 2

    def __init__(self, map_size=10.0, cell_size=0.05):
        self.map_size = map_size  # size (meter)
        self.cell_size = cell_size
        self.n_cell = map_size / cell_size
        self.grid = np.zeros((self.n_cell, self.n_cell), dtype=np.float32)
        self.grid -= 1

    def __get_grid_coord(self, x, y):
        grid_x = x / self.cell_size + self.n_cell / 2
        grid_y = y / self.cell_size + self.n_cell / 2
        return grid_x, grid_y

    def __get(self, grid_x, grid_y):
        if 0 <= grid_x < self.n_cell and 0 <= grid_y < self.n_cell:
            return float(self.grid[grid_y, grid_x])

    def __set(self, grid_x, grid_y, value):
        if 0 <= grid_x < self.n_cell and 0 <= grid_y < self.n_cell:
            self.grid[grid_y, grid_x] = value

    def __add(self, grid_x, grid_y, value):
        if 0 <= grid_x < self.n_cell and 0 <= grid_y < self.n_cell:
            self.grid[grid_y, grid_x] += value

    def sample(self, pose, sensor_values, cell_radius=1):
        weight = 0.0
        for sensor_x, sensor_y in sensor_values:
            # Grid coordinates
            dst_x, dst_y = local_to_global(sensor_x, sensor_y, pose)
            grid_dst_x, grid_dst_y = self.__get_grid_coord(dst_x, dst_y)
            grid_src_x, grid_src_y = self.__get_grid_coord(pose[0], pose[1])
            x_diff = grid_dst_x - grid_src_x
            y_diff = grid_dst_y - grid_src_y
            distance = math.sqrt(x_diff ** 2 + y_diff ** 2)
            if self.MAX_DEPTH_SQ < distance:
                continue

            # Positoin
            pos_found = False
            for y in xrange(int(grid_dst_y - cell_radius), int(grid_dst_y + cell_radius + 1)):
                for x in xrange(int(grid_dst_x - cell_radius), int(grid_dst_x + cell_radius + 1)):
                    value = self.__get(x, y)
                    if value > 30.0:
                        weight += 12.0
                        pos_found = True

            line_found = False
            if not pos_found:
                # Line
                theta = math.atan2(y_diff, x_diff)
                for d in xrange(int(distance)):
                    x = d * math.cos(theta) + grid_src_x
                    y = d * math.sin(theta) + grid_src_y
                    value = self.__get(x, y)
                    if value > 30.0:
                        weight += 4.0
                        line_found = True
                        break

            if not line_found:
                weight += 8.0

        return weight

    def update(self, pose, sensor_values):
        for sensor_x, sensor_y in sensor_values:
            # Grid coordinates
            dst_x, dst_y = local_to_global(sensor_x, sensor_y, pose)
            grid_dst_x, grid_dst_y = self.__get_grid_coord(dst_x, dst_y)
            grid_src_x, grid_src_y = self.__get_grid_coord(pose[0], pose[1])
            x_diff = grid_dst_x - grid_src_x
            y_diff = grid_dst_y - grid_src_y
            distance = math.sqrt(x_diff ** 2 + y_diff ** 2)
            if self.MAX_DEPTH_SQ < distance:
                continue

            # Positoin
            self.__add(grid_dst_x, grid_dst_y, 15.0)

            # Line
            theta = math.atan2(y_diff, x_diff)
            for d in xrange(int(distance)):
                x = d * math.cos(theta) + grid_src_x
                y = d * math.sin(theta) + grid_src_y
                self.__add(x, y, -1)


class Particle(object):
    # TODO(takiyu): Calibrate variance coeffs
    K1 = 0.05  # rot   -> rot
    K2 = 0.01  # trans -> rot
    K3 = 0.01  # trans -> trans
    K4 = 0.002  # rot   -> trans

    def __init__(self, pose):
        self.weight = 0.0
        self.pose = pose

    def copy(self):
        new = Particle(self.pose.copy())
        # new.weight = self.weight
        return new

    def sample_motion(self, odometry, pre_odometry):
        def normal(var):
            return random.gauss(0, math.sqrt(var))

        odometry_diff = odometry - pre_odometry
        if odometry_diff[0] == odometry_diff[1] == 0.0:
            if odometry_diff[2] == 0.0:
                return
            else:
                drot1 = odometry_diff[2]
                dtrans = 0.0
                drot2 = 0.0
        else:
            drot1 = math.atan2(odometry_diff[1], odometry_diff[0]) - pre_odometry[2]
            dtrans = math.sqrt(odometry_diff[0] ** 2 + odometry_diff[1] ** 2)
            drot2 = odometry_diff[2] - drot1

        sq_drot1 = drot1 ** 2
        sq_dtrans = dtrans ** 2
        sq_drot2 = drot2 ** 2

        # TODO(takiyu): Set offset
        drot1_hat = drot1 - normal(self.K1 * sq_drot1 + self.K2 * sq_dtrans)
        dtrans_hat = dtrans - normal(self.K3 * sq_dtrans + self.K4 * (sq_drot1 + sq_drot2))
        drot2_hat = drot2 - normal(self.K1 * sq_drot2 + self.K2 * sq_dtrans)

        self.pose = self.pose + np.array([dtrans_hat * math.cos(self.pose[2] + drot1_hat),
                                          dtrans_hat * math.sin(self.pose[2] + drot1_hat),
                                          drot1_hat + drot2_hat])

    def calc_weight(self, grid, sensor_values):
        self.weight = grid.sample(self.pose, sensor_values)


class GridParticleSlam(object):
    def __init__(self, n_particle=100):
        self.initialized = False
        self.n_particle = n_particle
        self.particles = [None] * n_particle
        self.grid = GridMap()
        self.pose = None
        self.pre_odometry = None

    def get_pose(self):
        return self.pose

    def get_grid_image(self):
        import cv2

        # Visualize grid
        grid = self.grid.grid
        image = np.zeros((grid.shape[0], grid.shape[1]), dtype=np.uint8) + 170
        for y in xrange(image.shape[0]):
            for x in xrange(image.shape[1]):
                belief = grid[y, x]
                if belief < -1.0:
                    image[y, x] = 255
                elif belief > 30.0:
                    image[y, x] = 0

        # Swap x-y axes
        M = np.array([[0, 1, 0], [1, 0, 0]], dtype=np.float32)
        image = cv2.warpAffine(image, M, (image.shape[0], image.shape[1]))

        return image

    def get_cell_size(self):
        return self.grid.cell_size

    def init_particles(self, pose):
        # init pose, pre_odometry and particles
        self.pose = pose
        self.pre_odometry = pose
        for i in xrange(self.n_particle):
            self.particles[i] = Particle(pose)
        self.initialized = True

    def normalize_particles(self):
        sum_w = 0.0
        for particle in self.particles:
            sum_w += particle.weight
        if sum_w == 0.0:
            w = 1.0 / self.n_particle
            for particle in self.particles:
                particle.weight = w
        else:
            for particle in self.particles:
                particle.weight /= sum_w

    def select_next_pose(self):
        # select mean
        # self.pose = np.zeros_like(self.pose)
        # for particle in self.particles:
        #     self.pose += particle.weight * particle.pose
        # self.pose[2] = bound_angle(self.pose[2])
        # select best
        best_particle = self.particles[0]
        best_weight = self.particles[0].weight
        for particle in self.particles[1:]:
            if particle.weight > best_weight:
                best_weight = particle.weight
                best_particle = particle
        self.pose = best_particle.pose
        self.pose[2] = bound_angle(self.pose[2])

    def resample_particles(self):
        next_particles = [None] * self.n_particle
        r = random.uniform(0.0, 1.0 / self.n_particle)
        c = self.particles[0].weight
        i = 1
        for m in xrange(self.n_particle):
            u = r + m / self.n_particle
            while u > c:
                i += 1
                c += self.particles[i].weight
            next_particles[m] = self.particles[i].copy()
        self.particles = next_particles

    def update(self, odometry, sensor_values):
        # init
        if not self.initialized:
            self.init_particles(odometry)
            return

        for particle in self.particles:
            # update motion
            particle.sample_motion(odometry, self.pre_odometry)
            # calc weight
            particle.calc_weight(self.grid, sensor_values)

        # normalize weights
        self.normalize_particles()

        # select next pose from particles
        self.select_next_pose()

        # update grid
        self.grid.update(self.pose, sensor_values)

        # resample
        self.resample_particles()

        # next status
        self.pre_odometry = odometry


class Slam(object):
    TAG = 'Slam'
    IMAGE_IDS = [0, 1]

    def __init__(self, robot):
        self.robot = robot

        self.slam_process = multiprocessing.Process(target=self.slam_loop)
        self.io_thread = threading.Thread(target=self.io_loop)
        self.slam_process.daemon = True
        self.io_thread.daemon = True
        self.io_event = multiprocessing.Event()
        self.sensor_queue = multiprocessing.Queue()
        self.result_queue = multiprocessing.Queue()

        self.fp_depth = FeaturePointDepth()
        self.slam = GridParticleSlam()

        # Output variables
        self.cell_size = 0.05

        self.fp_depth_cls = FeaturePointDepth

    def get_cell_size(self):
        return self.cell_size

    def io_loop(self):
        while True:
            # Wait for next request
            self.io_event.wait()
            self.io_event.clear()

            # Output result
            if not self.result_queue.empty():
                pose, encoded_grid, cell_size = self.result_queue.get()
                self.robot.movement.set_corrected_pose(pose)
                self.robot.save_grid_remote(encoded_grid)
                self.cell_size = cell_size

            # Get sensor values
            odometry = self.robot.movement.get_latest_odometry()
            images = self.robot.camera.capture(self.IMAGE_IDS)

            # Send sensor values
            self.sensor_queue.put((odometry, images))

    def slam_loop(self):
        import cv2

        while True:
            # Request sensor values
            self.io_event.set()
            # Wait for returning
            odometry, images = self.sensor_queue.get()
            odometry = np.array(odometry)

            # Image to depth
            if images[0].shape != (1,) and images[1].shape != (1,):
                ret = self.fp_depth.detect(images[0], images[1])
                if ret is None:
                    self.robot.log(self.TAG, "Invalid images")
                    ground_pts = []
                else:
                    pt1s, pt2s, depths = ret
                    # Convert to ground base coordinates
                    ground_pts = self.fp_depth.cvt_ground(images[0], pt1s, depths)
            else:
                self.robot.log(self.TAG, "Invalid images")
                ground_pts = []

            # Start slam
            self.slam.update(odometry, ground_pts)

            # Output result pose
            pose = self.slam.get_pose()
            grid_image = self.slam.get_grid_image()
            ret, encoded_grid = cv2.imencode('.jpg', grid_image)
            cell_size = self.slam.get_cell_size()
            self.result_queue.put((pose.tolist(), encoded_grid, cell_size))

            # Debug
#             im_depth = draw_depth_points(images[0], pt1s, depths)
#             cv2.imshow('im_depth', im_depth)
#             cv2.waitKey(100)

    def start(self):
        self.robot.log(self.TAG, 'start()')
        self.io_thread.start()
        self.slam_process.start()
        self.robot.save_grid_remote(DummySlam.ENCODED_GRID)

    def stop(self):
        self.robot.log(self.TAG, 'stop()')


class DummySlam(object):
    TAG = 'DummySlam'
    ENCODED_GRID = base64.b64decode('''
/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAIBAQEBAQIBAQECAgICAgQDAgICAgUEBAMEBgUGBgYFBgYG
BwkIBgcJBwYGCAsICQoKCgoKBggLDAsKDAkKCgr/wAALCADIAMgBAREA/8QAHwAAAQUBAQEBAQEAAAAA
AAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEI
I0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1
dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi
4+Tl5ufo6erx8vP09fb3+Pn6/9oACAEBAAA/AP38oooooooooooooooooooooooooooooooooooooooo
oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
oooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo
oooooooooooooooooooooooor//Z''')

    def __init__(self, robot):
        self.robot = robot
        self.cell_size = 0.05
        self.fp_depth_cls = FeaturePointDepth

    def get_cell_size(self):
        return self.cell_size

    def start(self):
        self.robot.log(self.TAG, 'start()')
        self.robot.save_grid_remote(self.ENCODED_GRID)

    def stop(self):
        self.robot.log(self.TAG, 'stop()')
